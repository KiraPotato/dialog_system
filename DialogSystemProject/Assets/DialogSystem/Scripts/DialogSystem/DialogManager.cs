﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Potato.DialogSystem.View;
using Potato.DialogSystem.Data;

namespace Potato.DialogSystem
{
    using Actions = Dictionary<DialogActionType, ActionDelegate>;
    using AssetLocationToVisualSettings = Dictionary<string, DialogVisualSettings>;
    using AssetLocationToInstantiatedDialog = Dictionary<string, MainDialog>;

    [RequireComponent(typeof(CanvasScaler))]
    public class DialogManager : MonoBehaviour
    {
        private Actions actions;
        private int currentlyShownEntry;
        private DialogInput currentInput;
        private MainDialog currentDialog;

        private DialogEntryHandler entries;
        private Queue<DialogInput> dialogQueue = new Queue<DialogInput>();
        private AssetLocationToVisualSettings loadedAssets = new AssetLocationToVisualSettings();
        private AssetLocationToInstantiatedDialog instantiatedDialogs = new AssetLocationToInstantiatedDialog();

        public void Show(DialogInput input)
        {
            if (actions == null)
            {
                actions = new Actions
                {
                    { DialogActionType.NextEntry, NextEntryAction },
                    { DialogActionType.GoToId, GoToIdAction },
                    { DialogActionType.GoToIndex, GoToIndexAction },
                    { DialogActionType.Exit, ExitAction },
                };
            }

            dialogQueue.Enqueue(input);

            if (currentInput == null)
                ShowNextDialog();
        }

        private void ShowNextDialog()
        {
            if (dialogQueue == null || dialogQueue.Count == 0)
                return;

            currentInput = dialogQueue.Dequeue();

            TryFetchDialogAsync(currentInput.Data.VisualSettingsLocation, OnDialogLoaded);
        }

        private void TryFetchDialogAsync(string visualAssetsLocation, OnDialogLoadedDelegate onDialogLoaded)
        {
            if (loadedAssets.TryGetValue(visualAssetsLocation, out DialogVisualSettings visualSettings))
            {
                onSuccess(visualSettings);
            }
            else
            {
                AssetLoader.LoadObject<DialogVisualSettings>(visualAssetsLocation, onSuccess, onVisualsLoadFailed);
            }

            void onSuccess(DialogVisualSettings loadedSettings)
            {
                MainDialog dialog;

                if (instantiatedDialogs.TryGetValue(visualAssetsLocation, out dialog) == false)
                {
                    dialog = Instantiate(loadedSettings.MainDialogPrefab, transform);
                    dialog.Initialize(loadedSettings);

                    dialog.OnResponseClicked += OnResponseClicked;
                }

                dialog.Hide();

                onDialogLoaded?.Invoke(visualAssetsLocation, dialog);
            }
        }

        private void OnDialogLoaded(string visualAssetsLocation, MainDialog dialogReference)
        {
            entries = new DialogEntryHandler(currentInput.Data);
            currentDialog = dialogReference;
            ShowEntry(currentInput.StartingIndex);
            currentInput.OnOpened?.Invoke(currentInput.DialogId);
        }


        private void onVisualsLoadFailed(in AssetLoaderError error)
        {
            Debug.LogError(error.ErrorMessage);

            ShowNextDialog();
        }

        #region Actions
        private void OnResponseClicked(int index)
        {
            if (entries.TryFetchActions(currentlyShownEntry, index, out IReadOnlyList<DialogAction> actions))
            {
                RunActions(actions);
            }
        }

        private void RunActions(IReadOnlyList<DialogAction> actions)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                RunAction(actions[i]);
            }
        }

        private void RunAction(DialogAction action)
        {
            if (actions.TryGetValue(action.Type, out ActionDelegate actionDelegate))
            {
                actionDelegate(action.Extra);
            }
        }
        #endregion

        #region Show
        private void ShowEntry(int index)
        {
            if (entries.TryFetchInput(index, out DialogEntryData input))
            {
                currentDialog.Show(input);
                currentlyShownEntry = index;
            }
        }

        private void ShowEntry(string id)
        {
            if (entries.TryFetchInput(id, out DialogEntryData input, out int index))
            {
                currentDialog.Show(input);
                currentlyShownEntry = index;
            }
        }
        #endregion

        #region Action Handlers
        private void NextEntryAction(string extraData)
        {
            int index = currentlyShownEntry + 1;

            if (index >= entries.EntriesCount)
            {
                ExitAction(extraData);
            }
            else
            {
                ShowEntry(index);
            }
        }

        private void GoToIdAction(string extraData)
        {
            ShowEntry(extraData);
        }

        private void GoToIndexAction(string extraData)
        {
            if (int.TryParse(extraData, out int index))
            {
                ShowEntry(index);
            }
        }

        private void ExitAction(string extraData)
        {
            currentDialog.Hide();

            var id = currentInput.DialogId;
            var callback = currentInput.OnClosed;

            currentDialog = null;
            currentInput = null;

            ShowNextDialog();

            callback?.Invoke(id);
        }
        #endregion

        private void OnGUI()
        {
            if (Debug.isDebugBuild)
            {
                Rect rect = new Rect(0, 0, Screen.width, Screen.height / 10);

                if (GUI.Button(rect, "Load from JSON in buffer"))
                {
                    string buffer = GUIUtility.systemCopyBuffer;

                    if (string.IsNullOrWhiteSpace(buffer) == false)
                    {
                        DialogData data = Data.DialogData.FromJson(buffer);
                        var input = new DialogInput(data, null, null);
                        Show(input);
                    }
                }
            }
        }

        private delegate void OnDialogLoadedDelegate(string visualAssetsLocation, MainDialog dialogReference);
    }

    public delegate void OnDialogOpenedDelegate(string dialogId);
    public delegate void OnDialogClosedDelegate(string dialogId);
    internal delegate void ActionDelegate(string extraData);
}