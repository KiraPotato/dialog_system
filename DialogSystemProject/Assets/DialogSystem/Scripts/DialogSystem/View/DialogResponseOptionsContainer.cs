﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Potato.DialogSystem.Data;

namespace Potato.DialogSystem.View
{
    [RequireComponent(typeof(LayoutGroup))]
    internal class DialogResponseOptionsContainer : MonoBehaviour
    {
        internal event OnDialogResponseClickedDelegate OnClick;

        private List<DialogResponseOption> createdObjects;
        private DialogVisualSettings visualSettings;

        internal void Initialize(DialogVisualSettings visualSettings)
        {
            if (createdObjects == null)
                createdObjects = new List<DialogResponseOption>();

            if (visualSettings != this.visualSettings)
            {
                ClearCreatedObjects();
                this.visualSettings = visualSettings;
            }

            while (createdObjects.Count < visualSettings.PreCreatedDialogOptions)
            {
                CreateNewObject().Hide();
            }
        }

        internal void Show(IReadOnlyList<string> responses)
        {
            if (responses == null)
                return;

            int count = Mathf.Max(responses.Count, createdObjects.Count);

            for (int index = 0; index < count; index++)
            {
                bool hide = responses.Count <= index;

                if (hide)
                {
                    createdObjects[index].Hide();
                    continue;
                }

                bool create = createdObjects.Count <= index;

                if (create)
                {
                    CreateNewObject();
                }

                createdObjects[index].Show(index, responses[index]);
            }
        }

        internal void Hide(int? index = null)
        {
            if (index != null)
            {
                if (index > 0 && index < createdObjects.Count)
                {
                    createdObjects[(int)index].Hide();
                }
                else
                {
                    Debug.LogException(new System.ArgumentOutOfRangeException());
                }
            }
            else if (createdObjects != null)
            {
                foreach (DialogResponseOption option in createdObjects)
                {
                    option.Hide();
                }
            }
        }

        private DialogResponseOption CreateNewObject()
        {
            DialogResponseOption option = Instantiate(visualSettings.ResponseOptionPrefab, transform);

            option.OnClick -= OnOptionClicked;
            option.OnClick += OnOptionClicked;

            createdObjects.Add(option);

            return option;
        }

        private void OnOptionClicked(int index)
            => OnClick?.Invoke(index);

        private void ClearCreatedObjects()
        {
            if (createdObjects.Count != 0)
            {
                for (int i = 0; i < createdObjects.Count; i++)
                {
                    createdObjects[i].OnClick -= OnOptionClicked;
                    Destroy(createdObjects[i]);
                }

                createdObjects.Clear();
            }
        }
    }
}