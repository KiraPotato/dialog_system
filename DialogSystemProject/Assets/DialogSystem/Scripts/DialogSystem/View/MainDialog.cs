using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Potato.DialogSystem.Data;

namespace Potato.DialogSystem.View
{
    internal class MainDialog : MonoBehaviour
    {
        internal event OnDialogResponseClickedDelegate OnResponseClicked;

        [Header("Scene References")]
        [SerializeField] 
        private DialogResponseOptionsContainer responsesContainer = default;

        [SerializeField] 
        private GameObject NextIconContainer = default;

        [Header("Text")]
        [SerializeField] 
        private TextMeshProUGUI title =  default;

        [SerializeField] 
        private TextMeshProUGUI message = default;

        [Header("Buttons")]
        [SerializeField] 
        private Button nextButton = default;

        private void Awake()
        {
            nextButton.onClick.AddListener(OnNextClicked);
            responsesContainer.OnClick += OnResponse;
        }

        internal void Initialize(DialogVisualSettings visualSettings)
        {
            responsesContainer.Initialize(visualSettings);
            NextIconContainer.gameObject.SetActive(false);
        }

        internal void Show(DialogEntryData input)
        {
            gameObject.SetActive(true);

            UpdateResponses(input);

            title.text = input.Title;
            message.text = input.Message;

            StartCoroutine(UpdateLayout());
        }

        internal void Hide()
        {
            gameObject.SetActive(false);
        }

        private IEnumerator UpdateLayout()
        {
            yield return null;
            LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
        }

        private void UpdateResponses(DialogEntryData input)
        {
            IReadOnlyList<string> responses = input.Responses;
            bool hasResponses = responses != null && responses.Count > 0;

            if (hasResponses)
            {
                responsesContainer.Show(responses);
            }
            else
            {
                responsesContainer.Hide();
            }

            NextIconContainer.gameObject.SetActive(hasResponses == false);
        }

        private void OnResponse(int index) 
            => OnResponseClicked?.Invoke(index);

        private void OnNextClicked() 
            => OnResponse(Constants.NEXT_BUTTON_RESPONSE_VALUE);
    }
}