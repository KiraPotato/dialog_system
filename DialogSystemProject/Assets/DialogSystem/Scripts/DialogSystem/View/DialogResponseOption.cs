﻿using UnityEngine;
using UnityEngine.UI;

using TMPro;

namespace Potato.DialogSystem.View
{
    [RequireComponent(typeof(Button), typeof(TextMeshProUGUI))]
    internal class DialogResponseOption : MonoBehaviour
    {
        internal event OnDialogResponseClickedDelegate OnClick;

        private Button button;
        private TextMeshProUGUI text;
        private int index;

        private void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();
            button = GetComponent<Button>();

            button.onClick.AddListener(OnButtonClicked);
        }

        internal void Show(int index, string text)
        {
            this.text.text = text;
            this.index = index;

            gameObject.SetActive(true);
        }

        internal void Hide()
        {
            gameObject.SetActive(false);
        }

        private void OnButtonClicked()
        {
            OnClick?.Invoke(index);
        }
    }

    internal delegate void OnDialogResponseClickedDelegate(int index);
}