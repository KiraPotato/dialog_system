using System.Collections.Generic;
using UnityEngine;

namespace Potato.DialogSystem.Data
{
    [System.Serializable]
    public class DialogData
    {
        public IReadOnlyList<DialogEntry> Entries => entries;

        public string VisualSettingsLocation => visualAssetsLocation;
        public string Id => dialogId;

        [SerializeField]
        private string dialogId = default;

        [SerializeField] 
        private string visualAssetsLocation = default;

        [SerializeField] 
        private List<DialogEntry> entries = default;

        public string ToJson() 
            => JsonUtility.ToJson(this, true);

        public static DialogData FromJson(string json) 
            => JsonUtility.FromJson<DialogData>(json);
    }
}