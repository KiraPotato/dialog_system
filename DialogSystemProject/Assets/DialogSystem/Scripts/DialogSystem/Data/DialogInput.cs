﻿namespace Potato.DialogSystem.Data
{
    public class DialogInput
    {
        public string DialogId => Data.Id;

        public int StartingIndex { get; }
        public DialogData Data { get; }
        public OnDialogOpenedDelegate OnOpened { get; }
        public OnDialogClosedDelegate OnClosed { get; }

        public DialogInput(DialogData data,
            OnDialogOpenedDelegate onShown,
            OnDialogClosedDelegate onClosed,
            int startingIndex = 0)
        {
            StartingIndex = startingIndex;
            Data = data;
            OnOpened = onShown;
            OnClosed = onClosed;
        }
    }
}