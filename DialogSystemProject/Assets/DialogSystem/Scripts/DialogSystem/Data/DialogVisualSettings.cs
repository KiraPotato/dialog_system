﻿using UnityEngine;

using Potato.DialogSystem.View;

namespace Potato.DialogSystem.Data
{
    [CreateAssetMenu(fileName = Constants.SCRIPTABLE_OBJECT_NAME_DIALOG_VISUAL_SETTINGS, menuName = Constants.SCRIPTABLE_OBJECT_MENU_DIALOG_VISUAL_SETTINGS)]
    internal class DialogVisualSettings : ScriptableObject
    {
        internal int PreCreatedDialogOptions => preCreatedDialogOptions;
        internal DialogResponseOption ResponseOptionPrefab => dialogResponsePrefab;
        internal MainDialog MainDialogPrefab => mainDialogPrefab;

        [SerializeField, Range(0, 10)]
        private int preCreatedDialogOptions = default;

        [SerializeField] 
        private DialogResponseOption dialogResponsePrefab = default;

        [SerializeField] 
        private MainDialog mainDialogPrefab = default;
    }
}