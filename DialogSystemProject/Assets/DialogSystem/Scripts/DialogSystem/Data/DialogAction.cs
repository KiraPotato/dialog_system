﻿using UnityEngine;

namespace Potato.DialogSystem.Data
{
    [System.Serializable]
    public class DialogAction
    {
        public DialogActionType Type => type;
        public string Extra => extra;

        [SerializeField] 
        private DialogActionType type = default;

        [SerializeField]
        private string extra = default;
    }
}