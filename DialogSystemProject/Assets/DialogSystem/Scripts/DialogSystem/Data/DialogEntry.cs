﻿using System.Collections.Generic;
using UnityEngine;

namespace Potato.DialogSystem.Data
{
    [System.Serializable]
    public class DialogEntry
    {
        public string Id => entryId;
        public string Title => title;
        public string Message => message;

        public IReadOnlyList<DialogResponse> Responses => responses;
        public IReadOnlyList<DialogAction> NextButtonActions => onNextActions;

        [SerializeField] private string entryId = default;

        [Header("Text")]
        [SerializeField] 
        private string title = default;

        [SerializeField, TextArea] 
        private string message = default;

        [Header("Responses")]
        [SerializeField] 
        private List<DialogResponse> responses = default;

        [SerializeField] 
        private List<DialogAction> onNextActions = default;
    }
}