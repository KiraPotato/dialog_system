﻿using System.Collections.Generic;
using UnityEngine;

namespace Potato.DialogSystem.Data
{
    [System.Serializable]
    public class DialogResponse
    {
        public string Text => text;

        public IReadOnlyList<DialogAction> Actions => actions;

        [SerializeField]
        private string text = default;

        [SerializeField] 
        private List<DialogAction> actions = default;
    }
}