﻿using System.Collections.Generic;

namespace Potato.DialogSystem.Data
{
    public class DialogEntryData
    {
        public IReadOnlyList<string> Responses { get; }
        public string Message { get; }
        public string Title { get; }

        public DialogEntryData(string title, string message, List<string> responses)
        {
            Title = title;
            Message = message;
            Responses = responses;
        }
    }
}