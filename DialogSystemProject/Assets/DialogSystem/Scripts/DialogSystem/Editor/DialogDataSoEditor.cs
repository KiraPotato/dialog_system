using UnityEngine;
using UnityEditor;

namespace Potato.DialogSystem.EditorScripts
{
    [CustomEditor(typeof(DialogDataSo))]
    public class DialogDataSoEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            DialogDataSo t = target as DialogDataSo;

            if (GUILayout.Button("Copy JSON to buffer"))
            {
                GUIUtility.systemCopyBuffer = t.GetSettingsJson();
            }
        }
    }
}