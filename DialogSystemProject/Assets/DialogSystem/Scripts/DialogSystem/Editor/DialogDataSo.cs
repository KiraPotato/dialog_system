﻿using Potato.DialogSystem.Data;
using UnityEngine;

namespace Potato.DialogSystem.EditorScripts
{
    [CreateAssetMenu]
    public class DialogDataSo : ScriptableObject
    {
        [SerializeField] DialogData jsonSettings = default;

        public string GetSettingsJson() => jsonSettings.ToJson();
    }
}