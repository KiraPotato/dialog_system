﻿using Potato.DialogSystem.Data;
using System.Collections.Generic;

namespace Potato.DialogSystem
{
    internal class DialogEntryHandler
    {
        internal int EntriesCount => entries.Length;

        private Entry[] entries;
        private Dictionary<string, int> idToIndex;

        internal DialogEntryHandler(DialogData input)
        {
            ChangeData(input);
        }

        internal void ChangeData(DialogData input)
        {
            Clear();

            int count = input.Entries.Count;
            entries = new Entry[count];
            idToIndex = new Dictionary<string, int>(count);

            for (int index = 0; index < count; index++)
            {
                DialogEntry entry = input.Entries[index];

                entries[index] = new Entry(entry);
                idToIndex[entry.Id] = index;
            }
        }

        internal void Clear()
        {
            entries = null;
            idToIndex = null;
        }

        internal bool TryFetchInput(int index, out DialogEntryData input)
        {
            if (TryFetchEntry(index, out Entry entry))
            {
                input = entry.input;
                return true;
            }

            input = default;
            return false;
        }

        internal bool TryFetchInput(string id, out DialogEntryData input, out int index)
        {
            if (idToIndex.TryGetValue(id, out index))
            {
                return TryFetchInput(index, out input);
            }
            else
            {
                input = default;
                return false;
            }
        }

        private bool TryFetchEntry(int index, out Entry entry)
        {
            if (index < 0 || index >= entries.Length)
            {
                entry = default;
                return false;
            }

            entry = entries[index];
            return true;
        }

        internal bool TryFetchActions(int entryIndex, int actionIndex, out IReadOnlyList<DialogAction> actions)
        {
            if (TryFetchEntry(entryIndex, out Entry entry)
                && entry.TryFetchActions(actionIndex, out actions))
            {
                return true;
            }

            actions = default;
            return false;
        }

        private class Entry
        {
            public DialogEntryData input { get; }

            private DialogEntry entry { get; }

            internal Entry(DialogEntry entry)
            {
                this.entry = entry;

                List<string> responses = null;

                if (entry.Responses != null && entry.Responses.Count > 0)
                {
                    responses = new List<string>(entry.Responses.Count);

                    foreach (var response in entry.Responses)
                    {
                        responses.Add(response.Text);
                    }
                }

                input = new DialogEntryData(entry.Title, entry.Message, responses);
            }

            internal bool TryFetchActions(int index, out IReadOnlyList<DialogAction> actions)
            {
                if (index >= 0 && index < entry.Responses.Count)
                {
                    actions = entry.Responses[index].Actions;
                    return true;
                }

                if (index == Constants.NEXT_BUTTON_RESPONSE_VALUE)
                {
                    actions = entry.NextButtonActions;
                    return true;
                }

                actions = default;
                return false;
            }
        }
    }
}