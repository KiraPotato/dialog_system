﻿using UnityEngine;

namespace Potato.DialogSystem
{
    internal static class Constants
    {
        #region Asset Loading
        internal const int ASSET_LOAD_FAIL_ERROR_ID = 0;
        internal const int ASSET_NULL_LOADED_ERROR_ID = 1;
        internal const int ASSET_FAIL_CAST_ERROR_ID = 2;
        internal const int ASSET_FAIL_EMPTY_ID = 3;
        internal const int ASSET_FAIL_NULL_SUCCESS_ID = 4;

        internal const string ERROR_LOAD_ASSET_FAILED_FORMAT = "Failed to load asset with ID {0}";
        internal const string ERROR_LOAD_ASSET_NULL_FORMAT = "Loaded asset with id {0} was null!";
        internal const string ERROR_LOAD_ASSET_CAST_FAIL_FORMAT = "Failed to cast asset id {0} to type {1}";
        internal const string ERROR_LOAD_ASSET_EMPTY_ID = "Can't load asset when ID is blank!";
        internal const string ERROR_LOAD_NULL_SUCCESS_CALLBACK = "Can't load asset when the success callback is null!";
        #endregion

        #region Scriptable Objects
        internal const string SCRIPTABLE_OBJECT_NAME_DIALOG_VISUAL_SETTINGS = "Dialog Visual Settings";

        internal const string SCRIPTABLE_OBJECT_MENU_ROOT = "Potato/Dialog System/";
        internal const string SCRIPTABLE_OBJECT_MENU_DIALOG_VISUAL_SETTINGS = SCRIPTABLE_OBJECT_MENU_ROOT + SCRIPTABLE_OBJECT_NAME_DIALOG_VISUAL_SETTINGS;
        #endregion

        internal const int NEXT_BUTTON_RESPONSE_VALUE = -1;
        internal static readonly Color BLANK_COLOR = new Color(0f, 0f, 0f, 0f);
    }
}