﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

using static Potato.DialogSystem.Constants;

namespace Potato.DialogSystem
{
    internal static class AssetLoader
    {
        // This simulates loading asset bundle from server async and then fetching the object from it
        internal static void LoadObject<TAssetType>(
            string assetId,
            OnAssetLoadedDelegate<TAssetType> onLoaded,
            OnAssetLoadedErrorDelegate onError)
        {
            if (string.IsNullOrWhiteSpace(assetId))
            {
                onError?.Invoke(new AssetLoaderError(
                    ASSET_FAIL_EMPTY_ID, ERROR_LOAD_ASSET_EMPTY_ID));

                return;
            }

            if (onLoaded == null)
            {
                onError?.Invoke(new AssetLoaderError(
                    ASSET_FAIL_NULL_SUCCESS_ID, ERROR_LOAD_NULL_SUCCESS_CALLBACK));

                return;
            }

            Addressables.LoadAssetAsync<object>(assetId).Completed += (AsyncOperationHandle<object> operation)
                 => OnLoaded(operation, assetId, onLoaded, onError);
        }

        private static void OnLoaded<TAssetType>(AsyncOperationHandle<object> obj,
            string assetId,
            OnAssetLoadedDelegate<TAssetType> onLoaded,
            OnAssetLoadedErrorDelegate onError)
        {
            if (obj.Status == AsyncOperationStatus.Failed)
            {
                onError?.Invoke(
                    AssetLoaderError.GetFormattedMessage(
                        ASSET_LOAD_FAIL_ERROR_ID,
                        ERROR_LOAD_ASSET_FAILED_FORMAT,
                        assetId));

                return;
            }

            object result = obj.Result;

            if (result == null)
            {
                onError?.Invoke(
                    AssetLoaderError.GetFormattedMessage(
                        ASSET_NULL_LOADED_ERROR_ID,
                        ERROR_LOAD_ASSET_NULL_FORMAT,
                        assetId));

                return;
            }

            TAssetType loadedAsset;
            bool success;

            try
            {
                if (typeof(TAssetType).IsSubclassOf(typeof(MonoBehaviour)))
                {
                    var go = result as GameObject;
                    loadedAsset = go.GetComponent<TAssetType>();
                }
                else
                {
                    loadedAsset = (TAssetType)result;
                }

                success = loadedAsset != null;
            }
            catch (System.Exception)
            {
                loadedAsset = default;
                success = false;
            }

            if (success)
            {
                onLoaded?.Invoke(loadedAsset);
            }
            else
            {
                onError?.Invoke(AssetLoaderError.GetFormattedMessage(
                    ASSET_FAIL_CAST_ERROR_ID,
                    ERROR_LOAD_ASSET_CAST_FAIL_FORMAT,
                    assetId,
                    typeof(TAssetType).Name));
            }
        }

        internal delegate void OnAssetLoadedDelegate<TAssetType>(TAssetType loadedAsset);
        internal delegate void OnAssetLoadedErrorDelegate(in AssetLoaderError error);
    }

    internal readonly struct AssetLoaderError
    {
        internal int Error { get; }
        internal string ErrorMessage { get; }

        internal AssetLoaderError(int error, string errorMessage)
        {
            Error = error;
            ErrorMessage = errorMessage;
        }

        internal static AssetLoaderError GetFormattedMessage(int error, string errorMessageFormat, params object[] args)
        {
            return new AssetLoaderError(error, string.Format(errorMessageFormat, args));
        }
    }
}