﻿namespace Potato.DialogSystem
{
    public enum DialogActionType
    {
        None = -1,

        /// <summary>
        /// Moves to the next entry in the list
        /// </summary>
        NextEntry = 0,

        /// <summary>
        /// Goes to the ID set in the action extra
        /// </summary>
        GoToId = 1,

        /// <summary>
        /// Goes to the entry index
        /// </summary>
        GoToIndex = 2,

        /// <summary>
        /// Exits dialog
        /// </summary>
        Exit = 3,
    }
}