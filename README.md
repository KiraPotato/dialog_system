
# Dialog  System

This  is a simple and expandable dialog system.

## Requirements
* TMPro
* Addressables (Will be replaced with proper bundle handling later)

## Details
The dialog system uses JSON parsed into objects to know what to show or how to behave.

To generate a JSON easily, you can use the Scriptable Object located in DialogSystem/DebugAssets.
There you can create the whole dialog and then export it to buffer.

To test this on any client, copy the JSON into buffer and during gameplay hit the
button appearing on the top of the screen. That will read the JSON and ask the dialog to load the assets and show the dialog.

Opening multiple dialogs will queue them up and will open them one after another.

## Accessing the Dialog
Place the Canvas from DialogSystem/DefaultDialogAssets/Canvas anywhere in the scene.
Then either use DI to inject the DialogManager into the required script or simply use unity SerializedField to achieve that.


### Other
For extra questions I'm available by mail at kirazpotato@gmail.com


# Changes
## Version 0.1
First version - Everything was created!